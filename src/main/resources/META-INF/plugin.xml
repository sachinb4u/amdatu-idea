<!--
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~    http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<idea-plugin>
    <id>org.amdatu.idea</id>
    <name>Amdatu</name>
    <version>3.0.1-SNAPSHOT</version>
    <description>The Amdatu IntelliJ plugin adds support for OSGi workspaces using the bnd workspace model.
    </description>
    <category>Framework integration</category>
    <vendor>The Amdatu Foundation</vendor>
    <idea-version since-build="201.6668.113"/>

    <depends>com.intellij.modules.java</depends>

    <project-components>
        <component>
            <interface-class>org.amdatu.idea.RepositoryValidationService</interface-class>
            <implementation-class>org.amdatu.idea.RepositoryValidationServiceImpl</implementation-class>
        </component>
        <component>
            <interface-class>org.amdatu.idea.AmdatuIdeaPlugin</interface-class>
            <implementation-class>org.amdatu.idea.AmdatuIdeaPluginImpl</implementation-class>
        </component>
        <component>
            <interface-class>org.amdatu.idea.BaseliningErrorService</interface-class>
            <implementation-class>org.amdatu.idea.BaseliningErrorServiceImpl</implementation-class>
        </component>
        <component>
            <implementation-class>org.amdatu.idea.PackageInfoService</implementation-class>
        </component>
    </project-components>

    <extensionPoints>
        <extensionPoint name="lang.bundledescriptor.parser.provider"
                        interface="org.amdatu.idea.lang.bundledescriptor.header.HeaderParserProvider"/>
    </extensionPoints>

    <extensions defaultExtensionNs="com.intellij">
        <!-- Initialize AmdatuIde plugin -->
        <postStartupActivity implementation="org.amdatu.idea.AmdatuIdeaPluginStartup"/>

        <!-- Project import -->
        <projectImportProvider implementation="org.amdatu.idea.imp.BndProjectImportProvider"/>

        <!-- Inspections -->
        <inspectionToolProvider implementation="org.amdatu.idea.inspections.AmdatuIdeaInspectionToolProvider"/>

        <toolWindow
                id="Repositories"
                icon="OsmorcIdeaIcons.Bnd"
                canCloseContents="false"
                anchor="right"
                factoryClass="org.amdatu.idea.toolwindow.RepositoriesToolWindowFactory">
        </toolWindow>

        <toolWindow
                id="Bundle info"
                icon="OsmorcIdeaIcons.Bnd"
                canCloseContents="false"
                anchor="bottom"
                factoryClass="org.amdatu.idea.toolwindow.BundleInfoToolWindowFactory">
        </toolWindow>

        <!-- Language -->
        <fileTypeFactory implementation="org.amdatu.idea.lang.bundledescriptor.BundleDescriptorTypeFactory"/>
        <lang.parserDefinition language="BundleDescriptor"
                               implementationClass="org.amdatu.idea.lang.bundledescriptor.parser.BundleDescriptorParserDefinition"/>
        <lang.syntaxHighlighterFactory language="BundleDescriptor"
                                       implementationClass="org.amdatu.idea.lang.bundledescriptor.highlighting.BundleDescriptorSyntaxHighlighterFactory"/>
        <annotator language="BundleDescriptor"
                   implementationClass="org.amdatu.idea.lang.bundledescriptor.highlighting.HeaderAnnotator"/>
        <lang.elementManipulator forClass="org.amdatu.idea.lang.bundledescriptor.psi.HeaderValuePart"
                                 implementationClass="org.amdatu.idea.lang.bundledescriptor.psi.impl.HeaderValuePartManipulator"/>
        <completion.contributor language="BundleDescriptor"
                                implementationClass="org.amdatu.idea.lang.bundledescriptor.completion.BundleDescriptorCompletionContributor"/>
        <completion.contributor language="BundleDescriptor"
                                implementationClass="org.amdatu.idea.lang.bundledescriptor.completion.BlueprintFeatureCompletionContributor"/>
        <completion.contributor language="BundleDescriptor"
                                implementationClass="org.amdatu.idea.lang.bundledescriptor.completion.BsnCompletionContributor"/>
        <completion.contributor language="BundleDescriptor"
                                implementationClass="org.amdatu.idea.lang.bundledescriptor.completion.PackageCompletionContributor"/>

        <applicationService
                serviceImplementation="org.amdatu.idea.lang.bundledescriptor.header.HeaderParserRepository"/>

        <lang.commenter language="BundleDescriptor"
                        implementationClass="org.amdatu.idea.lang.bundledescriptor.BundleDescriptorCommenter"/>

        <!-- JPS plugin -->
        <compileServer.plugin classpath="jps-plugin.jar;biz.aQute.bnd-5.3.0.jar"/>

        <!--  Run -->
        <configurationType implementation="org.amdatu.idea.run.BndRunConfigurationType"/>
        <runConfigurationProducer implementation="org.amdatu.idea.run.BndRunConfigurationProducer$Launch"/>
        <runConfigurationProducer implementation="org.amdatu.idea.run.BndRunConfigurationProducer$Test"/>

        <!-- Custom module type supporting bndtools templates-->
        <moduleType id="AMDATU_IDE_MODULE_TYPE" implementationClass="org.amdatu.idea.templating.AmdatuIdeaModuleType"/>

        <!-- Plugin preferences -->
        <applicationService serviceImplementation="org.amdatu.idea.preferences.AmdatuIdeaPreferences"/>
        <applicationConfigurable groupId="language" id="amdatu.ide.preferences" displayName="Amdatu"
                                 instance="org.amdatu.idea.preferences.AmdatuIdeaPreferencesEditor"/>

        <!-- Project view -->
        <projectViewNodeDecorator implementation="org.amdatu.idea.AmdatuIdeaProjectViewNodeDecorator"/>

    </extensions>

    <extensions defaultExtensionNs="org.amdatu.idea">
        <lang.bundledescriptor.parser.provider
                implementation="org.amdatu.idea.lang.bundledescriptor.header.impl.StandardManifestHeaderParsers"/>
        <lang.bundledescriptor.parser.provider
                implementation="org.amdatu.idea.lang.bundledescriptor.header.impl.BndHeaderParsers"/>
        <lang.bundledescriptor.parser.provider
                implementation="org.amdatu.idea.lang.bundledescriptor.header.impl.AmdatuBlueprintHeaderParsers"/>
    </extensions>


    <actions>
        <group popup="true" text="Amdatu" id="amdatu-idea" description="Amdatu">
            <add-to-group group-id="ToolsMenu" anchor="last"/>
        </group>

        <!-- Refresh workspace -->
        <action id="amdatu.ide.action.refresh-workspace" icon="OsmorcIdeaIcons.Bnd"
                text="Refresh workspace"
                description="Refresh workspace"
                class="org.amdatu.idea.actions.RefreshWorkspaceAction">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
        </action>

        <!-- Run all tests -->
        <action id="amdatu.ide.action.run-unit-tests" icon="OsmorcIdeaIcons.BndTest"
                text="Run unit tests"
                description="Run unit tests"
                class="org.amdatu.idea.actions.RunUnitTestsAction">
            <keyboard-shortcut first-keystroke="control alt U" keymap="$default"/>
            <add-to-group group-id="amdatu-idea" anchor="last"/>
        </action>

        <!-- Run all tests -->
        <action id="amdatu.ide.action.run-integration-tests" icon="OsmorcIdeaIcons.BndTest"
                text="Run integration tests"
                description="Run integration tests"
                class="org.amdatu.idea.actions.RunIntegrationTestsAction">
            <keyboard-shortcut first-keystroke="control alt I" keymap="$default"/>x
            <add-to-group group-id="amdatu-idea" anchor="last"/>
        </action>

        <!-- Refresh repositories -->
        <action id="amdatu.ide.action.refresh-repositories" icon="OsmorcIdeaIcons.Bnd"
                text="Refresh repositories"
                description="Refresh workspace"
                class="org.amdatu.idea.actions.RefreshRepositoriesAction">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
        </action>

        <!-- Generate index -->
        <action id="amdatu.ide.action.generate-index" icon="OsmorcIdeaIcons.Bnd"
                text="Generate index"
                description="Generate index"
                class="org.amdatu.idea.actions.index.GenerateIndexAction">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
            <add-to-group group-id="ProjectViewPopupMenu" anchor="last"/>
        </action>

        <!-- Update workspace -->
        <action id="amdatu.ide.action.update-workspace" icon="OsmorcIdeaIcons.Bnd"
                text="Update configuration project"
                description="Update configuration project from template"
                class="org.amdatu.idea.actions.UpdateConfigurationProjectAction">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
            <add-to-group group-id="ProjectViewPopupMenu" anchor="last"/>
        </action>

        <!-- Update workspace -->
        <action id="amdatu.ide.action.create-configuration" icon="OsmorcIdeaIcons.Bnd"
                text="Create configuration"
                description="Create configuration"
                class="org.amdatu.idea.actions.CreateConfigurationAction">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
            <add-to-group group-id="ProjectViewPopupMenu" anchor="last"/>
        </action>
        <!-- Export bndrun -->
        <action id="amdatu.ide.action.export-executable-jar" icon="OsmorcIdeaIcons.Bnd"
                text="Export executable jar"
                description="Export executable jar"
                class="org.amdatu.idea.actions.ExportExecutableJar">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
            <add-to-group group-id="ProjectViewPopupMenu" anchor="last"/>
        </action>
        <!-- Check for Amdatu Blueprint update -->
        <action id="amdatu.ide.action.blueprint.check-for-update" icon="OsmorcIdeaIcons.Bnd"
                text="Check for Amdatu Blueprint update"
                description="Check for Amdatu Blueprint update"
                class="org.amdatu.idea.actions.CheckForBlueprintUpdate">
            <add-to-group group-id="amdatu-idea" anchor="last"/>
            <add-to-group group-id="ProjectViewPopupMenu" anchor="last"/>
        </action>

        <!-- Fix baselining errors -->
        <action id="amdatu.ide.action.fix-baselining-errros" icon="OsmorcIdeaIcons.Bnd"
                text="Fix baselining errors"
                description="Fixes baselining errors reported by the build messages"
                class="org.amdatu.idea.actions.FixBaseliningErrorsAction">
            <add-to-group group-id="CompilerErrorViewPopupMenu" anchor="last"/>
        </action>
    </actions>
</idea-plugin>
